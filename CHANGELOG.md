Change Log
==========

## 3.2.0

- Adds ability to query and render multiple `pageList` and `pageStats` elements in the DOM.
- Adds two new demos

## 3.1.0

- Bump core dependency to 3.1.2, improves version comparison functionality.

## 3.0.0

- Release
